#!/usr/bin/env node
import inquirer from 'inquirer'
import { $, argv } from 'zx'

import { fileURLToPath } from 'url'
import { dirname } from 'path'

import inquirerFileTreeSelection from 'inquirer-file-tree-selection-prompt'

// 🔈 Quite mode
$.verbose = false

inquirer.registerPrompt('file-tree-selection', inquirerFileTreeSelection)

const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

//#region Constants
const GET_TYPES = {
  COMPONENT: 'component',
}

// const VALIDATION_TYPES = {
//   common: ['pathname'],
//   component: ['name', 'layer', 'slice'],
// }
//#endregion Constants

const { genType } = argv.genType
  ? argv
  : await inquirer.prompt([
      {
        type: 'list',
        message: 'Select type',
        name: 'genType',
        choices: ['component', 'entity', 'feature', 'lib', 'page'],
      },
    ])

const { pathname } = argv?.pathname
  ? argv
  : await inquirer.prompt([
      {
        type: 'file-tree-selection',
        name: 'pathname',
        onlyShowDir: true,
      },
    ])

//#region General utils
const getParam = async ({ nameField }) => {
  if (argv?.[nameField]) return Promise.resolve(argv?.[nameField])

  const payload = await inquirer.prompt([
    {
      type: 'input',
      message: `Input ${nameField}`,
      name: nameField,
    },
  ])

  return payload[nameField]
}
//#endregion General utils

//#region Declaration generation functions
const generateComponent = async ({ name, pathname, layer, slice }) => {
  // TODO: add validation

  await $`HYGEN_TMPLS=${__dirname}/_templates npx hygen ${GET_TYPES.COMPONENT} new --pathname=${pathname} --name=${name} --layer=${layer} --slice=${slice}`

  return undefined
}
//#endregion Declaration generation functions

//#region Main launching
if (genType === GET_TYPES.COMPONENT) {
  const name = await getParam({ nameField: 'name' })
  const layer = await getParam({ nameField: 'layer' })
  const slice = await getParam({ nameField: 'slice' })

  await generateComponent({ pathname, name, layer, slice })
}
//#endregion Main launching
