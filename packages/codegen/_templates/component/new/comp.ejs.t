---
to: <%= pathname %>/<%= h.changeCase.param(name) %>.tsx
---
import clsx from 'clsx'

import styles from './<%= h.changeCase.param(name) %>.module.css'

export interface I<%= name %>Props {}

export const <%= name %> = ({}: I<%= name %>Props) => {
  return <div className={clsx(styles.wrap)}>{}</div>
}
