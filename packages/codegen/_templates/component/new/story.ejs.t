---
to: <%= pathname %>/<%= h.changeCase.param(name) %>.stories.tsx
---
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { <%= name %> } from './<%= h.changeCase.param(name) %>'

export default {
  title: '<%= layer %>/<%= slice %>/<%= name %>',
  component: <%= name %>,
} as ComponentMeta<typeof <%= name %>>

const Template: ComponentStory<typeof <%= name %>> = (args) => <<%= name %> {...args} />

export const Default = Template.bind({})

Default.args = {}
