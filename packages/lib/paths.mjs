import { realpathSync } from 'fs'
import { join, normalize, dirname } from 'path'
import { fileURLToPath } from 'url'

const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

export const srcDir = normalize(join(__dirname, '..'))
export const rootDir = normalize(join(srcDir, '..'))
export const appDir = normalize(realpathSync(process.cwd()))
export const tmpDir = normalize(join(rootDir, '/tmp'))
