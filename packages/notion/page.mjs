import { Readability, isProbablyReaderable } from '@mozilla/readability'
import { paramCase } from 'change-case'
import { JSDOM } from 'jsdom'
import playwright from 'playwright'
import TurndownService from 'turndown'
import { fs, chalk } from 'zx'

const getHtmlPlaywright = async (url) => {
  const browser = await playwright.chromium.launch()
  const context = await browser.newContext()
  const page = await context.newPage()
  await page.goto(url)
  const html = await page.content()
  await browser.close()

  return html
}

const getDocumentFromStringHtml = (html, url) => {
  const doc = new JSDOM(html, {
    url,
  })

  return doc
}

const setMetadata = (kv) => {
  const startTemplate = `--- \n`
  const endTemplate = `--- \n\n`

  let content = ''

  for (const [key, value] of Object.entries(kv)) {
    content += `${key}: ${value} \n`
  }

  return startTemplate + content + endTemplate
}

// add jsdoc
export const generateMarkdownContentFile = async ({ spinner, notionDir, url, kv }) => {
  // const fileName = paramCase(kv.title)
  let markdown = ''

  spinner.text = `Generating Markdown file: ${chalk.yellow(kv.Title)} ${chalk.blue(url)}`

  try {
    if (url) {
      const html = await getHtmlPlaywright(url)
      const document = getDocumentFromStringHtml(html, url)

      let article

      if (isProbablyReaderable(document.window.document)) {
        article = new Readability(document.window.document).parse()
      }

      // 🔗 https://github.com/mixmark-io/turndown#option
      const turndownService = new TurndownService()
      markdown = turndownService.turndown(article?.content || '<h1>Error!</h1>')
    }

    const meta = setMetadata(kv)

    // const file = await fs.readFile('filename.md')
    await fs.writeFile(`${notionDir}/${kv.Title}.md`, meta + markdown, 'utf8')
  } catch (_error) {}
}
