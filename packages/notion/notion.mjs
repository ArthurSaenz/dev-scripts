import { Client } from '@notionhq/client'
import { chalk, sleep } from 'zx'

const articleTypes = {
  personalNote: 'personalNote',
  webArticle: 'webArticle',
  rootNote: 'rootNote',
}

const getNotionItemData = async ({ cursor, notion, databaseId }) => {
  const query = {
    database_id: databaseId,
    start_cursor: cursor || undefined,
    sorts: [
      {
        property: 'Created',
        direction: 'descending',
      },
    ],
  }

  const response = await notion.databases.query(query)

  const { has_more, next_cursor, results } = response

  let data = []

  for await (const page of results) {
    const roots = await Promise.all(
      page.properties['Referred by (root)'].relation.map((element) =>
        notion.pages
          .retrieve({ page_id: element.id })
          .then((response) => {
            return '[[' + response.properties.Title.title[0]?.plain_text + ']]' || ''
          })
          .catch(() => ''),
      ),
    )

    await sleep(50)

    const childs = await Promise.all(
      page.properties['Related (child)'].relation.map((element) =>
        notion.pages
          .retrieve({ page_id: element.id })
          .then((response) => {
            return '[[' + response.properties.Title.title[0]?.plain_text + ']]' || ''
          })
          .catch(() => ''),
      ),
    )

    await sleep(50)

    data.push({
      id: page.id,
      Title: page.properties.Title.title[0]?.plain_text,
      URL: page.properties.URL.url,
      Edited: page.last_edited_time,
      Сreated: page.created_time,
      // Evaluation: page.properties['Оценка'],
      Type: page.properties.Article.formula.boolean ? articleTypes.webArticle : '',
      Tags: page.properties.Tags.multi_select.map((element) => element.name).join(', '),
      Category: page.properties.Category.multi_select.map((element) => element.name).join(', '),
      Root: roots.join(', '),
      Children: childs.join(', '),
    })
  }

  return { has_more, next_cursor, data }
}

let count = 1
export const launch = async ({ spinner, cursor, data, notion, databaseId }) => {
  try {
    spinner.text = `Notion API: fetching ${chalk.yellow('#' + count)}`

    const payload = await getNotionItemData({ cursor, notion, databaseId })

    count += 1

    const newData = [...data, ...payload.data]

    if (payload.has_more) {
      await sleep(100)

      return launch({
        spinner,
        cursor: payload.next_cursor,
        data: newData,
        notion,
        databaseId,
      })
    }

    return newData
  } catch (error) {
    console.log('\n', 'Error')
    console.log(error)
  }
}

export const getNotionData = async ({ spinner, cursor, data }) => {
  const notion = new Client({ auth: process.env.NOTION_API_KEY })

  const databaseId = process.env.NOTION_API_DATABASE

  const result = await launch({
    spinner,
    cursor,
    data: data || [],
    notion,
    databaseId,
  })

  return result
}
