#!/usr/bin/env node
import { $, chalk, fs, sleep } from 'zx'
import { realpathSync } from 'fs'
import { normalize } from 'path'
import ora from 'ora'
import dotenv from 'dotenv'

import { generateMarkdownContentFile } from './page.mjs'
import { getNotionData } from './notion.mjs'

dotenv.config()

const spinner = ora().start()

// const data = await getNotionData({ spinner })

// spinner.succeed(`Notion API: ${chalk.green.bold(data.length)} ${chalk.blue('Finished!')}`)

const currentDir = normalize(realpathSync(process.cwd()))

const notionDir = `${currentDir}/notion`

// await $`rm -Rf ${notionDir}`
// await $`mkdir -p ${notionDir}`

// try {
//   fs.writeFileSync(notionDir + '/_notion_data.json', JSON.stringify(data, null, 4))
//   console.log(`Notion Data -> ${chalk.blue('_notion_data.json')} ${chalk.green('done!')}`)
// } catch (error) {
//   console.error('🚨 Error writing -> notion_data.json', error)
// }

spinner.start()

const fileData = fs.readFileSync(notionDir + '/_notion_data.json')

// Phase #2
for await (const kv of JSON.parse(fileData)) {
  const url = kv.URL

  await generateMarkdownContentFile({ spinner, notionDir, url, kv })

  await sleep(1000)
}

spinner.succeed(`Generated all ${chalk.green.bold(data.length)} pages were ${chalk.blue('Finished!')}`)
