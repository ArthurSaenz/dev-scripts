# Install all workspace deps

### Install git

```bash
sudo apt install -y git
```

### Configure git config

```bash
git config --global user.name "Arthur Saenko"
git config --global user.email "arthur.saenz7@gmail.com"

git config --global core.editor "code --wait"
```



### Install zsh and oh-my-zsh

>  ⚠️ Install only after node.js

```bash
sudo apt install -y zsh

sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```


### Install the Make util

```bash
sudo apt install -y build-essential
```
