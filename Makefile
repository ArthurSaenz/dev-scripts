help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)


##@ Setup (install and bootstrap)

setup:	## Install all dependencies and built the artifacts
	yarn install --silent && make install-dotenv-lint && make build-packages

reinstall:	## Clear cache and reinstall all dependencies and rebuild artifacts
	yarn cache clean && make setup

build-packages:	## Build all packages
	yarn workspaces foreach -ip run build

clean-artifacts:	## Clean dist (artifacts) in all packages
	yarn workspaces foreach -pt run clean

clean-cache:	## Clean all caches
	yarn workspaces foreach -pt run clean

upgrade-deps:	## Open the upgrade interface.
	yarn upgrade-interactive

upgrade-yarn:	## Upgrade Yarn 3 to the the latest version.
	yarn set version latest

install-dotenv-lint:	## Install dotenv-linter tool
	curl -sSfL https://raw.githubusercontent.com/dotenv-linter/dotenv-linter/master/install.sh | sh -s -- -b ./env/bin

dedupe-check:	## Check for duplicates (can be used as a CI step)
	yarn dedupe --check

dedupe-fix:	## Dedupe all packages using a specific strategy
	yarn dedupe

doctor:	## Statically analyze your sources to try to locate the most common issues that could result in a subpar experience
	yarn dlx @yarnpkg/doctor


##@ General

build: ## Run build packages of libraries (scripts package)
	yarn run scripts build

docs:	## Run documentation locally on http://localhost:4444
	yarn workspace docs-app run serve

commit:	## Customize git commit
	yarn pnpify git-cz


##@ Code quality

dotenv-linter:	## Run linting for env files
	cd env && bin/dotenv-linter

dotenv-linter-local:	## Run compare (linting) for env files from reference file
	cd env && bin/dotenv-linter compare .env.reference .env.local

stylelint:	## Run stylelint
	yarn workspaces foreach -pv run stylelint

format:	## Launch prettier with format all files
	yarn workspaces foreach -pv run format

codestyle:	## Launch prettier only check files. Without formating files
	yarn workspaces foreach -pv run codestyle

eslint:	## Launch eslint linting (with cache .eslintcache)
	yarn workspaces foreach -pv run eslint

eslint-report:	## Launch coverage report of eslint rules
	yarn workspaces foreach -pv run eslint-report

lint:	## Launch all linting tools
	yarn workspaces foreach -pv run lint

fix:	## Launch combine script for all fixing. That script must be contain the eslint --fix && stylelint --fix
	yarn workspaces foreach -pv run fix

qa:	## Launch combine script with all testing scripts: from eslint to jest
	yarn workspaces foreach -pv run qa

test:	## ## Launch all tests
	yarn workspaces foreach -pv run test

test-watch:	## Launch all unit tests in watch mode
	yarn workspaces foreach -pv run test-watch

test-silent:	## Launch yarn test-silent script
	yarn workspaces foreach -pv run test-silent

test-report:	## Launch test-report script with jest --coverage
	yarn workspaces foreach -pv run test-report

ts-check:	## Launch Typescript compilator with --check flag
	yarn workspaces foreach -pv run ts-check

ts-report:	## Launch coverage report of type definition
	yarn workspaces foreach -pv run ts-report